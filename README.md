# GO echo SSE app
[![Go version](https://img.shields.io/static/v1?label=Go&message=v1.14.15&color=blue)](https://golang.org/)

Project use **gomodules**

Download source code
```shell script
git clone https://gitlab.com/it.devel.att/go-echo-event-sourcing.git
cd go-echo-event-sourcing/
```

Launch app via docker-compose
```shell script
make docker-up-local
```
After you can go to http://localhost/ for see result

Or you can start app local
```shell script
make run-redis
make run
```
After it you can go to http://localhost:8080/ (8080 is default port) for see result


## Config variables
* **SERVER_PORT**

Development server port

Default value **:8080** Notice **:**
* **LOG_LEVEL**

Log level of [logrus logger library](https://github.com/sirupsen/logrus). Possible values
| Value             | Level       |
| ----------------- | ----------- |
| panic             | PanicLevel  |
| fatal             | FatalLevel  |
| error             | ErrorLevel  |
| warn              | WarnLevel   |
| warning           | WarnLevel   |
| info              | InfoLevel   |
| debug             | DebugLevel  |
| trace             | TraceLevel  |
* **REDIS_DSN**

Url to redis host

Default value **redis://127.0.0.1:6379/0**
* **ECHO_SEND_INTERVAL**

Set interval of send a message to client
Default value **1s**
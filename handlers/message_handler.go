package handlers

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"go-echo-event-sourcing/repositories"
	"go-echo-event-sourcing/settings"
	"go-echo-event-sourcing/subscribers"
	"net/http"
)

type MessageHandler struct {
	shared             settings.Shared
	messageRepo        repositories.MessageRepository
	messagesSubscriber subscribers.Subscriber
}

func NewMessageHandler(
	shared settings.Shared,
	messageRepo repositories.MessageRepository,
	messagesSubscriber subscribers.Subscriber,
) MessageHandler {

	return MessageHandler{
		shared:             shared,
		messageRepo:        messageRepo,
		messagesSubscriber: messagesSubscriber,
	}
}

func (mh MessageHandler) Echo(w http.ResponseWriter, r *http.Request) {
	log.Info("[MessageHandler.Echo] Accept new connection")
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "http not support flusher", http.StatusInternalServerError)
		return
	}

	message := r.URL.Query().Get("w")
	userID := r.URL.Query().Get("user_id")

	messagesCh := make(chan string)
	w.Header().Set("User-ID", userID)

	if err := mh.messageRepo.SetMessage(userID, message); err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mh.messagesSubscriber.Subscribe(r.Context(), userID, messagesCh)

	defer close(messagesCh)
	for {
		select {
		case message := <-messagesCh:
			if _, err := fmt.Fprintf(w, "data: %s\n\n", message); err != nil {
				log.Error(err)
			}
			flusher.Flush()
		case <-r.Context().Done():
			log.Info("[MessageHandler.Echo] Receive application context done")
			return
		case <-mh.shared.Context().Done():
			log.Info("[MessageHandler.Echo] Receive request context done")
			return
		}
	}
}

func (mh MessageHandler) Say(w http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("User-Id")
	log.Infof("[MessageHandler.Say] UserID %v", userID)
	if userID == "" {
		w.WriteHeader(401)
		return
	}
	message := r.URL.Query().Get("w")
	if message != "" {
		if err := mh.messageRepo.SetMessage(userID, message); err != nil {
			w.WriteHeader(400)
			return
		}
	}
	w.WriteHeader(200)
}

package subscribers

import "context"

type Subscriber interface {
	Subscribe(requestContext context.Context, userID string, messages chan<- string)
}

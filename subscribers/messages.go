package subscribers

import (
	"context"
	log "github.com/sirupsen/logrus"
	"go-echo-event-sourcing/repositories"
	"go-echo-event-sourcing/settings"
)

type messagesSubscriber struct {
	shared         settings.Shared
	messageRepo    repositories.MessageRepository
}

func NewMessagesSubscriber(
	shared settings.Shared,
	messageRepo repositories.MessageRepository,
) Subscriber {
	return messagesSubscriber{
		shared:         shared,
		messageRepo:    messageRepo,
	}
}

func (s messagesSubscriber) Subscribe(requestContext context.Context, userID string, messages chan<- string) {
	s.shared.WaitGroup().Add(1)

	go func() {
		defer s.shared.WaitGroup().Done()

		for {
			select {
			case <-s.shared.Context().Done():
				log.Infof("[messagesSubscriber.Subscribe] Receive application context done")
				return
			case <-requestContext.Done():
				log.Infof("[messagesSubscriber.Subscribe] Receive request context done")
				return
			case <-s.shared.Time().Tick(s.shared.Config().GetEchoSendInterval()):
				message, err := s.messageRepo.GetMessage(userID)
				if err != nil {
					log.Error(err)
				} else {
					messages <- message
				}
			}
		}
	}()
}

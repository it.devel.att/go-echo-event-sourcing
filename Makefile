PROJECT_NAME=echo-sse-app

env:
	cp .env.example .env

run-redis:
	docker-compose run -d redis_db

run:
	go run main.go

build:
	go build -o ${PROJECT_NAME} main.go

docker-build:
	docker-compose build echo-sse-app

docker-push:
	docker-compose push

docker-up-local:
	TAG=local-demo docker-compose up -d

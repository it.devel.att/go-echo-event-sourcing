FROM golang:1.14.15-alpine AS builder

RUN apk --no-cache add git gcc
WORKDIR /src
COPY . .

ARG APP_VERSION=""
RUN go build -i -ldflags "-X echo-sse-app/config.AppVersion=$APP_VERSION" -o /echo-sse-app main.go

FROM alpine:3.10

COPY --from=builder /echo-sse-app /echo-sse-app
COPY --from=builder /src/static /static

ENTRYPOINT ["/echo-sse-app"]
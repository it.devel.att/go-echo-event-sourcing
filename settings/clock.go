package settings

import "time"

// For mocking purpose of time package

type Clock interface {
	Now() time.Time
	After(d time.Duration) <-chan time.Time
	Tick(d time.Duration) <-chan time.Time
}
type clock struct{}

func NewClock() Clock {
	return clock{}
}

func (c clock) Now() time.Time {
	return time.Now()
}

func (c clock) After(d time.Duration) <-chan time.Time {
	return time.After(d)
}

func (c clock) Tick(d time.Duration) <-chan time.Time {
	return time.Tick(d)
}

package main

import (
	"context"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"go-echo-event-sourcing/config"
	"go-echo-event-sourcing/db"
	"go-echo-event-sourcing/handlers"
	"go-echo-event-sourcing/repositories"
	"go-echo-event-sourcing/settings"
	"go-echo-event-sourcing/subscribers"
	"net/http"
	"os"
)

func init() {
	if _, err := os.Stat(".env"); err == nil {
		if err := godotenv.Load(); err != nil {
			log.Infof("[main.init] Error while try to godotenv.Load: %v", err)
		}
	} else {
		log.Infof("[main.init] File .env not exists. Pass to load env variables from file")
	}
}

func initLogger(cfg config.Config) {
	logLevel, err := log.ParseLevel(cfg.GetLogLevel())
	if err != nil {
		log.SetLevel(log.DebugLevel)
		log.Errorf("Error while trying to parse level from config: %v", err)
	} else {
		log.SetLevel(logLevel)
	}
}

func main() {
	ctx, _ := context.WithCancel(context.Background())
	cfg := config.NewConfig()
	initLogger(cfg)

	shared := settings.NewShared(ctx, cfg)
	redisClient := db.NewRedisClient(shared)

	messageRepo := repositories.NewMessageRepository(redisClient)
	messagesSubscriber := subscribers.NewMessagesSubscriber(shared, messageRepo)
	messageHandler := handlers.NewMessageHandler(shared, messageRepo, messagesSubscriber)

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)
	http.HandleFunc("/echo", messageHandler.Echo)
	http.HandleFunc("/say", messageHandler.Say)

	log.Info("[main] Start application")
	if err := http.ListenAndServe(cfg.GetServerPort(), nil); err != nil {
		log.Fatal(err)
	}
}

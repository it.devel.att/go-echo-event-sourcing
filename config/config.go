package config

import (
	"github.com/caarlos0/env/v6"
	log "github.com/sirupsen/logrus"
	"time"
)

var AppVersion = "development"

type (
	Config interface {
		GetServerPort() string
		GetLogLevel() string
		GetRedisDSN() string
		GetEchoSendInterval() time.Duration
	}

	config struct {
		ServerPort       string        `env:"SERVER_PORT" envDefault:":8080"`
		LogLevel         string        `env:"LOG_LEVEL" envDefault:"info"`
		RedisDSN         string        `env:"REDIS_DSN" envDefault:"redis://127.0.0.1:6379/0"`
		EchoSendInterval time.Duration `env:"ECHO_SEND_INTERVAL" envDefault:"1s"`
	}
)

func NewConfig() Config {
	cfg := &config{}
	if err := env.Parse(cfg); err != nil {
		log.Fatal(err)
	}
	return cfg
}

func (c config) GetServerPort() string {
	return c.ServerPort
}

func (c config) GetLogLevel() string {
	return c.LogLevel
}

func (c config) GetRedisDSN() string {
	return c.RedisDSN
}

func (c config) GetEchoSendInterval() time.Duration {
	return c.EchoSendInterval
}

package db

import (
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"go-echo-event-sourcing/settings"
)

func NewRedisClient(shared settings.Shared) *redis.Client {
	opt, err := redis.ParseURL(shared.Config().GetRedisDSN())
	if err != nil {
		log.Fatal(err)
	}
	return redis.NewClient(opt)
}

package repositories

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

var userMessageKey = "user:%v:message"

type (
	MessageRepository interface {
		GetMessage(userID string) (string, error)
		SetMessage(userID string, message string) error
	}

	messageRepository struct {
		client *redis.Client
	}
)

func NewMessageRepository(client *redis.Client) MessageRepository {

	return messageRepository{
		client: client,
	}
}

func (r messageRepository) GetMessage(userID string) (string, error) {
	key := fmt.Sprintf(userMessageKey, userID)

	value, err := r.client.Get(context.Background(), key).Result()
	if err != nil {
		return "", err
	}
	return value, nil
}

func (r messageRepository) SetMessage(userID string, message string) error {
	key := fmt.Sprintf(userMessageKey, userID)

	if err := r.client.Set(context.Background(), key, message, time.Hour*24).Err(); err != nil {
		return err
	}
	return nil
}
